import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    public sallaryRanges: Array<any>;
	public ageRanges: Array<any>
	public educations: Array<any>
	public types:Array <any>;
public name:string = '';
public ageRange: {start:0, end:0};

	public startAge=0;
	public endAge;
	public sallaryRange:{start:0, end:0};
	public startSallary=0;	
	public endSallary;
	public professionalBackground:string='';
	public insuranceType:string='';
	public insuranceCompany:string='';
	public education:string='';
	public insuranceStartDate;
	public insuranceEndDate;
	public date={};
  constructor() {
	this.types =[];
this.types=['car', 'health', 'life']	
this.sallaryRanges=[];
  this.sallaryRanges=[
  {start:0,
  end:500},
  {start:500,
  end:1000}, 
  {start:1000,
  end:2000}, 
  {start:2000,
  end:4000}, 
  {start:4000,
  end:100000}]
  this.ageRanges=[];
  this.ageRanges=[
  {start:0,
  end:18}, 
  {start:18,
  end:25}, 
  {start:25,
  end:40},
  {start:40,
  end:64},
  {start:64,
  end:100} ]
  //this.educations=[];
  this.educations=['highschool', 'bachelor', 'master', 'phd']
  this.date ={
	  startDate:this.insuranceStartDate,
	  endDate:this.insuranceEndDate
  }
  }

search() {
	if (this.startAge ==0) {
		this.startAge=this.ageRange.start;
		this.endAge=this.ageRange.end;
			
	}
	if(this.startSallary ==0) {
		this.startSallary=this.sallaryRange.start;
		this.endSallary = this.sallaryRange.end;
	}
alert(this.startAge);
	//alert(this.name);
	//alert(JSON.stringify(this.ageRange));
	//alert(JSON.stringify(this.sallaryRange));
	//alert(this.education);
	//alert(this.professionalBackground)
	
}
}
