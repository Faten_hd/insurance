import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
public clients: Array <any>
  constructor(public http:HttpClient) {
	  this.clients=[];
	  	    this.http.get('http://insur.lcp.website/api/clients.php').subscribe((res) => {
this.clients = res["data"];
//alert(this.clients);
});	

  }

}
