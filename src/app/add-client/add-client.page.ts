import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {
	  clientForm: FormGroup;  
	public educations: Array<any>
	public types:any  =[];
	public companies:Array <any>;
public firstName:string = '';
public lastName:string = '';
public phoneNumber:number;

	public dateOfBirth:Date;
	public sallary=0;	
	public professionalBackground:string='';
	public insuranceType:string;
	public insuranceCompany:string;
	public education:string;
	public insuranceStartDate:Date;
	public insuranceEndDate;
	public date={};	

  constructor(public http:HttpClient, public formBuilder:FormBuilder) {
	  	this.types =[];
    this.http.get('http://insur.lcp.website/api/insurance-types.php').subscribe((res) => {
this.types=res["data"];		 
		//alert(this.types);
	});

	
  this.educations=[];
  this.http.get('http://insur.lcp.website/api/educations.php').subscribe((res) => {
	 this.educations=res["data"]
  });

  this.companies=[];
  this.http.get('http://insur.lcp.website/api/companies.php').subscribe((res) => {
	 this.companies=res["data"]
	 //alert(JSON.stringify(	this.companies))
  });

	  }

  ngOnInit() {
	      this.clientForm = this.formBuilder.group({
			  firstName:'',
			  lastName:'',
			  age:0,
			  phoneNumber:0,
			  sallary:0,
			  professionalBackground:'',
			  insuranceType:'',
			  insuranceCompany:''
		  });
			  
  }

addClient() {
	var data = new FormData();
	
		data.append('firstName',this.firstName)
	data.append('lastName',this.lastName);
		data.append('dateOfBirth',this.dateOfBirth.toString());
			data.append('salary',this.sallary.toString());
				data.append('professionalBackground',this.professionalBackground);
					data.append('phoneNumber',this.phoneNumber.toString());
						data.append('insuranceType',this.insuranceType);
							data.append('insuranceCompany',this.insuranceCompany);
								data.append('education',this.education);
									data.append('insuranceDate',this.insuranceStartDate.toString());
	this.http.post('http://insur.lcp.website/api/insert.php',data).subscribe(res => {
	//alert(JSON.stringify(res))}, err=> {
	/*alert(JSON.stringify(err))*/});

	
}
}
