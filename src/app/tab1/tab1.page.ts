import { Component } from '@angular/core';
	import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
	public clients:Array <any>
	public searchResult:Array<any>
    public sallaryRanges: Array<any>;
	public ageRanges: Array<any>
	public educations: Array<any>
	public types:any  =[];
	public companies:Array <any>;
public firstName:string;
public lastName:string;
public phoneNumber:number;

	public age:Date;
	public sallary;	
	public professionalBackground:string;
	public insuranceType:string;
	public insuranceCompany:string;
	public education:string;
	public insuranceStartDate;
	public insuranceEndDate;
	public date={};	
  constructor(public http:HttpClient) {
	this.types =[];
    this.http.get('http://insur.lcp.website/api/insurance-types.php').subscribe((res) => {
this.types=res["data"];		 
		//alert(this.types);
	});

	
  this.educations=[];
  this.http.get('http://insur.lcp.website/api/educations.php').subscribe((res) => {
	 this.educations=res["data"]
  });

  this.companies=[];
  this.http.get('http://insur.lcp.website/api/companies.php').subscribe((res) => {
	 this.companies=res["data"]
  });
  this.clients=[];
  this.http.get('http://insur.lcp.website/api/clients.php').subscribe((res) => {
this.clients = res["data"];
//alert(this.clients);
});	

  this.date ={
	  startDate:this.insuranceStartDate,
	  endDate:this.insuranceEndDate
  }
  }

search() {

	this.searchResult=[]; //an array where i want to push the search result 
	/*
	this.searchResult = this.clients.filter(function(item) {

return 
item.firstName ==this.firstName || item.lastName==this.lastName || item.phoneNumber==this.phoneNumber || item.salary==this.sallary || item.insuranceType==this.insuranceType || item.insuranceCompany==this.insuranceCompany|| item.education==this.education || item.professionalBackground ==this.professionalBackground 
});
alert(this.searchResult);
*/
	
	
	this.clients.forEach(client => { 

	if (client.firstName ==this.firstName || client.lastName==this.lastName || client.age==this.age || client.professionalBackground==this.professionalBackground ||client.insuranceCompany==this.insuranceCompany || client.salary==this.sallary ||client.insuranceType==this.insuranceType || client.phoneNumber==this.phoneNumber || client.insuranceDate ==this.insuranceStartDate) { 
	this.searchResult.push(client);
}	
	}); 
	
			var id = document.getElementById("1");
		if (id.style.display === "none") {
			id.style.display = "block";	
		}
		

}
}
