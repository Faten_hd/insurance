import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
public clients: Array<any>
  constructor(public http:HttpClient) {
	  this.clients=[];
	    this.http.get('http://insur.lcp.website/api/clients.php').subscribe((res) => {
this.clients = res["data"];
//alert(this.clients);
});	

  }

}
